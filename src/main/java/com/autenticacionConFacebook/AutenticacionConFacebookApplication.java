package com.autenticacionConFacebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutenticacionConFacebookApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutenticacionConFacebookApplication.class, args);
	}

}
