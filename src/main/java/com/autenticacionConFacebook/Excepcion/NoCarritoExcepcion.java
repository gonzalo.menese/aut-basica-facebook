package com.autenticacionConFacebook.Excepcion;

public class NoCarritoExcepcion extends Exception{

    public NoCarritoExcepcion(String mensaje) {
        super(mensaje);
    }
}
