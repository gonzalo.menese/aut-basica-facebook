package com.autenticacionConFacebook.controlador;

import com.autenticacionConFacebook.dto.CarritoDtoPrivado;
import com.autenticacionConFacebook.dto.CarritoDtoPublico;
import com.autenticacionConFacebook.entidad.Carrito;
import com.autenticacionConFacebook.servicio.CarritoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/carritos")
public class CarritoControlador {

    private CarritoService carritoService;

    @Autowired
    public CarritoControlador(CarritoService carritoService){
        this.carritoService = carritoService;
    }

    @GetMapping
    public List<CarritoDtoPrivado> obtenerCarritosDto(){
        return carritoService.obtenerCarritosDto();
    }
    @PostMapping
    public void insertarCarrito(@RequestBody CarritoDtoPublico carritoDtoPublico){
        carritoService.insertarCarrito(carritoDtoPublico);
    }

    @DeleteMapping("/{id}")
    public void eliminarCarrito(@PathVariable("id") int id) throws Exception{
        carritoService.eliminarCarrito(id);
    }

    @PutMapping("/{id}")
    public void modificarCarrito(@RequestBody CarritoDtoPublico carritoDtoPublico, @PathVariable("id") int id)throws Exception{
        carritoService.modificarCarrito(id, carritoDtoPublico);
    }

    @GetMapping("/{id}")
    public Carrito obtenerUnCarrito(@PathVariable("id") int id)throws Exception{
        return carritoService.obtenerCarrito(id);
    }

}
