package com.autenticacionConFacebook.controlador;

import com.autenticacionConFacebook.entidad.CarritoProducto;
import com.autenticacionConFacebook.entidad.Producto;
import com.autenticacionConFacebook.servicio.CarritoProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/carritoproducto")

public class CarritoProductoControlador {
    CarritoProductoService carritoProductoService;

    @Autowired
    public CarritoProductoControlador(CarritoProductoService carritoProductoService){
        this.carritoProductoService = carritoProductoService;
    }

    @GetMapping
    public List<CarritoProducto> obtenerCarritoProductos(){
        return carritoProductoService.obtenerCarritoProductos();
    }


    @GetMapping("/{idCarrito}")
    List<Producto> obtenerProductosDeCarrito(@PathVariable("idCarrito") int idCarrito) throws Exception {
        return carritoProductoService.obtenerProductosPorCarrito(idCarrito);
    }

    @PostMapping
    public void insertarCarritoProducto(@RequestBody Map<String, String> body){

        carritoProductoService.insertarCarritoProducto(body);
    }

    @DeleteMapping("/{idCarrito}")
    public void eliminarProdDeCarrito(@PathVariable("id") int idCarrito, @RequestBody Map<String, String> body){
        carritoProductoService.eliminarProdDeCarrito(idCarrito, body);
    }

}

