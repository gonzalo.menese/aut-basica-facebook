package com.autenticacionConFacebook.controlador;

import com.autenticacionConFacebook.entidad.Producto;
import com.autenticacionConFacebook.servicio.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/productos")
public class ProductoControlador {

    ProductoService productoService;

    @Autowired
    public ProductoControlador(ProductoService productoService){
        this.productoService = productoService;
    }
    @GetMapping
    public List<Producto> obtenerProductos() {
        return productoService.obtenerProductos();
    }

    @PostMapping
    public void insertarProducto(@RequestBody Map<String,String> body){
        productoService.insertarProducto(body);
    }

    @DeleteMapping("/{id}")
    public void eliminarProducto(@PathVariable("id") int id) throws Exception{
        productoService.eliminarProducto(id);
    }

    @PutMapping("/{id}")
    public void modificarProducto(@PathVariable("id") int id,
                                  @RequestBody Map<String,String> body) throws Exception{

        productoService.modificarProducto(id, body);
    }


}
