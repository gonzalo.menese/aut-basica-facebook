package com.autenticacionConFacebook.dto;

public class CarritoDtoPrivado {


    private int id;
    private String nombreUsuarioCarrito;
    private int cantidadProductos;
    private boolean estado;

    public CarritoDtoPrivado(){

    }

    public CarritoDtoPrivado(String nombreUsuarioCarrito, int cantidadProductos){
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
        this.cantidadProductos = cantidadProductos;
        this.estado = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreUsuarioCarrito() {
        return nombreUsuarioCarrito;
    }

    public void setNombreUsuarioCarrito(String nombreUsuarioCarrito) {
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
    }

    public int getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(int cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
