package com.autenticacionConFacebook.dto;

public class CarritoDtoPublico {
    private String nombreUsuarioCarrito;
    private int cantidadProductos;

    public CarritoDtoPublico(){

    }

    public CarritoDtoPublico(String nombreUsuarioCarrito, int cantidadProductos){
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
        this.cantidadProductos = cantidadProductos;
    }

    public String getNombreUsuarioCarrito() {
        return nombreUsuarioCarrito;
    }

    public void setNombreUsuarioCarrito(String nombreUsuarioCarrito) {
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
    }

    public int getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(int cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }
}
