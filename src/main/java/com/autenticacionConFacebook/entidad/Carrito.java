package com.autenticacionConFacebook.entidad;

import javax.persistence.*;

@Entity
public class Carrito {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="nombre_usuario_carrito")
    private String nombreUsuarioCarrito;

    @Column(name="cantidad_productos")
    private int cantidadProductos;

    @Column(name="estado")
    private boolean estado;

    public Carrito(){}

    public Carrito(String nombreUsuarioCarrito, int cantidadProductos){
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
        this.cantidadProductos = cantidadProductos;
        this.estado = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreUsuarioCarrito() {
        return nombreUsuarioCarrito;
    }

    public void setNombreUsuarioCarrito(String nombreUsuarioCarrito) {
        this.nombreUsuarioCarrito = nombreUsuarioCarrito;
    }

    public int getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(Integer cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }

    public void setEstado(boolean estado){
        this.estado = estado;
    }

    public boolean getEstado(){
        return this.estado;
    }
}
