package com.autenticacionConFacebook.entidad;

import javax.persistence.*;

@Entity
public class CarritoProducto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="id_carrito")
    private int idCarrito;

    @Column(name="id_producto")
    private int idProducto;

    @Column(name = "estado")
    private boolean estado;

    public CarritoProducto(){}

    public CarritoProducto(int idCarrito, int idProducto){
        this.idCarrito = idCarrito;
        this.idProducto = idProducto;
        this.estado = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCarrito() {
        return idCarrito;
    }

    public void setIdCarrito(Integer idCarrito) {
        this.idCarrito = idCarrito;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
