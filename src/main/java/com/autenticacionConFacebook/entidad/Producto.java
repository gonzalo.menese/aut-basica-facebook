package com.autenticacionConFacebook.entidad;

import javax.persistence.*;

@Entity
public class Producto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="codigo")
    private int codigo;

    @Column(name="nombre")
    private String nombre;

    @Column(name="precio")
    private int precio;

    @Column(name="estado")
    private boolean estado;

    public Producto(){}

    public Producto(int codigo,String nombre,int precio){
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.estado = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
