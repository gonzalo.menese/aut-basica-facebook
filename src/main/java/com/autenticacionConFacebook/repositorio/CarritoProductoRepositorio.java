package com.autenticacionConFacebook.repositorio;

import com.autenticacionConFacebook.entidad.CarritoProducto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarritoProductoRepositorio extends JpaRepository<CarritoProducto,Integer> {
    List<CarritoProducto> findByIdCarritoAndEstado(int idCarrito, boolean estado);
    CarritoProducto findByIdCarritoAndIdProducto(int idCarrito, int idProducto);
    List<CarritoProducto> findByEstado(boolean estado);
}
