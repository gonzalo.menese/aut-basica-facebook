package com.autenticacionConFacebook.repositorio;

import com.autenticacionConFacebook.entidad.Carrito;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarritoRepositorio extends JpaRepository<Carrito,Integer> {
    List<Carrito> findByEstado(boolean estado);
    Carrito findByNombreUsuarioCarrito(String NombreUsuarioCarrito);
}
