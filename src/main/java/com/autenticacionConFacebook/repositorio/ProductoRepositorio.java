package com.autenticacionConFacebook.repositorio;

import com.autenticacionConFacebook.entidad.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductoRepositorio extends JpaRepository<Producto,Integer> {
    List<Producto> findByEstado(boolean estado);
}
