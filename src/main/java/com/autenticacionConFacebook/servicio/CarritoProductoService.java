package com.autenticacionConFacebook.servicio;


import com.autenticacionConFacebook.entidad.CarritoProducto;
import com.autenticacionConFacebook.entidad.Producto;
import com.autenticacionConFacebook.repositorio.CarritoProductoRepositorio;
import com.autenticacionConFacebook.repositorio.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CarritoProductoService {
    CarritoProductoRepositorio carritoProductoRepositorio;
    ProductoRepositorio productoRepositorio;

    @Autowired
    public CarritoProductoService(CarritoProductoRepositorio carritoProductoRepositorio, ProductoRepositorio productoRepositorio){
        this.carritoProductoRepositorio = carritoProductoRepositorio;
        this.productoRepositorio = productoRepositorio;
    }

    public List<CarritoProducto> obtenerCarritoProductos(){
        return carritoProductoRepositorio.findByEstado(true);
    }

    public CarritoProducto insertarCarritoProducto(CarritoProducto carritoProducto){
        return carritoProductoRepositorio.save(carritoProducto);
    }

    public List<Producto> obtenerProductosPorCarrito(int idCarrito) throws Exception {
        List<CarritoProducto> lista = carritoProductoRepositorio.findByIdCarritoAndEstado(idCarrito, true);
        List<Producto> productos = new ArrayList<>();
        for(int i = 0; i<lista.size(); i++){
            Optional<Producto> productoRepo = productoRepositorio.findById(lista.get(i).getIdProducto());
            if(!productoRepo.isPresent()){
                throw new Exception("no existe este producto");
            }
            productos.add(productoRepo.get());
        }
        return productos;
    }

    public CarritoProducto eliminarProdCarrito(int idCarrito, int idProducto){
        CarritoProducto carritoProducto = carritoProductoRepositorio.findByIdCarritoAndIdProducto(idCarrito, idProducto);
        carritoProducto.setEstado(false);
        return carritoProductoRepositorio.save(carritoProducto);
    }

    public CarritoProducto insertarCarritoProducto(Map<String, String> body){
        int idCarrito = Integer.parseInt(body.get("idCarrito"));
        int idProducto = Integer.parseInt(body.get("idProducto"));
        return insertarCarritoProducto(new CarritoProducto(idCarrito, idProducto));
    }

    public CarritoProducto eliminarProdDeCarrito(int idCarrito, Map<String, String> body){
        int idProducto = Integer.parseInt(body.get("idProducto"));
        return eliminarProdCarrito(idCarrito, idProducto);
    }
}
