package com.autenticacionConFacebook.servicio;

import com.autenticacionConFacebook.Excepcion.NoCarritoExcepcion;
import com.autenticacionConFacebook.dto.CarritoDtoPrivado;
import com.autenticacionConFacebook.dto.CarritoDtoPublico;
import com.autenticacionConFacebook.entidad.Carrito;
import com.autenticacionConFacebook.repositorio.CarritoRepositorio;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarritoService {


    private CarritoRepositorio carritoRepositorio;


    @Autowired
    public CarritoService(CarritoRepositorio carritoRepositorio){
        this.carritoRepositorio = carritoRepositorio;
    }


    public CarritoDtoPrivado obtenerCarritoDto(int id) throws NoCarritoExcepcion {
        Optional<Carrito> carrito = carritoRepositorio.findById(id);
        if(!carrito.isPresent()){
            throw new NoCarritoExcepcion("No hay carrito con ese id");
        }
        else{
            return convertirADto(carrito.get());
        }
    }

    public Carrito obtenerCarrito(int id) throws NoCarritoExcepcion {
        Optional<Carrito> carrito = carritoRepositorio.findById(id);
        if(!carrito.isPresent()){
            throw new NoCarritoExcepcion("No hay carrito con ese id");
        }
        else{
            return carrito.get();
        }
    }

    public Carrito insertarCarrito(Carrito carrito){
        return carritoRepositorio.save(carrito);
    }

    private List<Carrito> obtenerCarritos(){
        return carritoRepositorio.findByEstado(true);
    }

    public List<CarritoDtoPrivado> obtenerCarritosDto(){
        List<Carrito> carritos = obtenerCarritos();
        List<CarritoDtoPrivado> carritosDto = new ArrayList<>();
        CarritoDtoPrivado carritoDtoPrivado;
        for(int i = 0; i<carritos.size(); i++){
            carritoDtoPrivado = convertirADto(carritos.get(i));
            carritosDto.add(carritoDtoPrivado);
        }
        return carritosDto;
    }

    public void eliminarCarrito(int id) throws Exception{
        Carrito carrito = obtenerCarrito(id);
        carrito.setEstado(false);
        insertarCarrito(carrito);
    }

    public Carrito insertarCarrito(CarritoDtoPublico carritoDtoPublico){
        Carrito carrito = convertirAEntidad(carritoDtoPublico);
        carrito.setEstado(true);
        return insertarCarrito(carrito);
    }

    public Carrito modificarCarrito(int id, CarritoDtoPublico carritoDtoPublico) throws Exception{

        Carrito obtenido = obtenerCarrito(id);
        Carrito carrito = convertirAEntidad(carritoDtoPublico);
        if(carrito.getNombreUsuarioCarrito() == ""){
            carrito.setNombreUsuarioCarrito(obtenido.getNombreUsuarioCarrito());
        }
        if(carrito.getCantidadProductos() == 0){
            carrito.setCantidadProductos(obtenido.getCantidadProductos());
        }
        carrito.setId(id);
        carrito.setEstado(obtenido.getEstado());

        return insertarCarrito(carrito);

    }

    private Carrito convertirAEntidad(CarritoDtoPublico carritoDtoPublico){
        ModelMapper modelMapper = new ModelMapper();
        Carrito carrito = modelMapper.map(carritoDtoPublico, Carrito.class);
        return carrito;
    }

    private CarritoDtoPrivado convertirADto(Carrito carrito){
        ModelMapper modelMapper = new ModelMapper();
        CarritoDtoPrivado carritoDtoPrivado = modelMapper.map(carrito, CarritoDtoPrivado.class);
        return carritoDtoPrivado;
    }


}
