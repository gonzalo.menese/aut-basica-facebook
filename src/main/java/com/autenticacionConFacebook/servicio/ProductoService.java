package com.autenticacionConFacebook.servicio;

import com.autenticacionConFacebook.entidad.Producto;
import com.autenticacionConFacebook.repositorio.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    ProductoRepositorio productoRepositorio;

    public ProductoService(ProductoRepositorio productoRepositorio){
        this.productoRepositorio = productoRepositorio;
    }

    public Producto obtenerProducto(int id) throws Exception{
        Optional<Producto> producto = productoRepositorio.findById(id);
        if(producto.isPresent()){
            return producto.get();
        }
        else{
            throw new Exception();
        }
    }

    public Producto insertarProducto(Producto producto){
        return productoRepositorio.save(producto);
    }

    public Producto eliminarProducto(int id) throws Exception{
        Producto producto = obtenerProducto(id);
        producto.setEstado(false);
        return insertarProducto(producto);
    }

    public List<Producto> obtenerProductos(){
        return productoRepositorio.findByEstado(true);
    }

    public Producto insertarProducto(Map<String, String> body){
        String nombre = body.get("nombre");
        int codigo = Integer.parseInt(body.get("codigo"));
        int precio = Integer.parseInt(body.get("precio"));
        return insertarProducto(new Producto(codigo, nombre, precio));
    }

    public void modificarProducto(int id, Map<String, String> body) throws Exception{

        Producto producto = this.obtenerProducto(id);
        if(body.containsKey("codigo"))
        {
            producto.setCodigo( Integer.parseInt(body.get("codigo")));
        }
        if(body.containsKey("nombre"))
        {
            producto.setNombre(body.get("nombre"));
        }
        if(body.containsKey("precio"))
        {
            producto.setPrecio(Integer.parseInt(body.get("precio")));
        }
        insertarProducto(producto);
    }

}
