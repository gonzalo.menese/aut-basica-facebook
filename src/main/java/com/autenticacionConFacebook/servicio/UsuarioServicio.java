package com.autenticacionConFacebook.servicio;

import com.autenticacionConFacebook.entidad.Rol;
import com.autenticacionConFacebook.entidad.Usuario;
import com.autenticacionConFacebook.repositorio.UsuarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UsuarioServicio implements UserDetailsService {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Override
    public UserDetails loadUserByUsername(String nombreUsuario) throws UsernameNotFoundException {
        Optional<Usuario> usuario = usuarioRepositorio.findByNombre(nombreUsuario);
        if(usuario.isPresent()) {
            List<GrantedAuthority> roles = convertirRolesAListaGrantedAuthority(usuario.get().getRoles());

            UserDetails userDetails = new User(usuario.get().getNombre(),
                    usuario.get().getClave(), roles);
            return userDetails;
        }
        throw new UsernameNotFoundException("Usuario no encontrado");
    }

    private SimpleGrantedAuthority convertirRolASimpleGrantedAuthority(Rol rol){
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(rol.getAutorizacion());
        return simpleGrantedAuthority;
    }

    private List<GrantedAuthority> convertirRolesAListaGrantedAuthority(Set<Rol> roles){
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for(Rol rol: roles){
            grantedAuthorities.add(convertirRolASimpleGrantedAuthority(rol));
        }
        return grantedAuthorities;
    }
}
